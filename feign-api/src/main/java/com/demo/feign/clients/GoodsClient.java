package com.demo.feign.clients;


import com.demo.feign.pojo.Goods;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "goodsService")
public interface GoodsClient {

    @GetMapping("/goods/{id}")
    Goods getInfo(@PathVariable("id") Long id);
}
