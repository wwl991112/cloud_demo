package com.demo.feign.pojo;

import lombok.Data;

@Data
public class User {
    private Long id;
    private String name;
    private String password;
    private int age;
    private String phone;
    private Integer state;
}