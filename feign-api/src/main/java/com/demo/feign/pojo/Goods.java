package com.demo.feign.pojo;

import lombok.Data;

@Data
public class Goods {
    private Long id;
    private String name;
    private Double price;
    private String brand;
    private String brief;
    private Integer state;
}