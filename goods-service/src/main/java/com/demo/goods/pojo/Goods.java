package com.demo.goods.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "goods")
public class Goods {
    @TableId
    private Long id;
    private String name;
    private Double price;
    private String brand;
    private String brief;
    private Integer state;
}