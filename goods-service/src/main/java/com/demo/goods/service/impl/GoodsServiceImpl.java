package com.demo.goods.service.impl;

import com.demo.goods.mapper.GoodsMapper;
import com.demo.goods.pojo.Goods;
import com.demo.goods.service.GoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {


}