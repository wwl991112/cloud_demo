package com.demo.goods.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName(value = "user")
public class User {
    @TableId
    private Long id;
    private String name;
    private String password;
    private int age;
    private String phone;
    private Integer state;
}