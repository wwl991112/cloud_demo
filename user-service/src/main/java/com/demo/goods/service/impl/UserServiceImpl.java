package com.demo.goods.service.impl;

import com.demo.goods.mapper.UserMapper;
import com.demo.goods.pojo.User;
import com.demo.goods.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {


}