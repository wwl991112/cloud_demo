package com.demo.order;

import com.netflix.loadbalancer.AvailabilityFilteringRule;
import com.netflix.loadbalancer.IRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfig {
    @Bean
    public IRule ribbonRule() {
        return new AvailabilityFilteringRule(); // 使用可用性过滤规则
    }
}
