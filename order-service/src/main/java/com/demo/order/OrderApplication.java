package com.demo.order;

import com.demo.feign.clients.GoodsClient;
import com.demo.feign.clients.UserClient;
import com.demo.feign.config.DefaultFeignConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@MapperScan("com.demo.order.mapper")
@SpringBootApplication
@EnableFeignClients(clients = {UserClient.class, GoodsClient.class},defaultConfiguration = DefaultFeignConfiguration.class)
//@EnableEurekaClient
//@RibbonClient(name = "orderService", configuration = RibbonConfig.class)
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}