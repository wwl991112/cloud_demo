package com.demo.order.service;

import com.demo.order.pojo.Order;
import com.baomidou.mybatisplus.extension.service.IService;

public interface OrderService extends IService<Order> {
    Order getInfo(Long id);
}
