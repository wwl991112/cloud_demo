package com.demo.order.service.impl;

import com.demo.feign.clients.GoodsClient;
import com.demo.feign.clients.UserClient;
import com.demo.order.mapper.OrderMapper;
import com.demo.order.pojo.Order;
import com.demo.order.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {


    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private UserClient userClient;

    @Autowired
    private GoodsClient goodsClient;

    @Override
    public Order getInfo(Long id) {
        Order order = orderMapper.selectById(id);
        // Feign远程调用
        if (order != null){
            order.setUser(userClient.getInfo(order.getUserId())).setGoods(goodsClient.getInfo(order.getGoodId()));
        }
        return order;
    }
}
