package com.demo.order.pojo;

import com.demo.feign.pojo.Goods;
import com.demo.feign.pojo.User;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName(value = "orders")
public class Order {
    @TableId
    private Long id;
    private Long userId;
    private Long goodId;
    private String address;
    private Integer state;

    @TableField(exist = false)
    private User user;
    @TableField(exist = false)
    private Goods goods;
}